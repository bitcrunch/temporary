/**
 * Created by dzambuto on 02/10/15.
 */
var store = require('../')
  , should = require('should');

describe('temporary module:', function () {
  var token;

  it('crea una connessione di default', function () {
    should.exists(store);
    should.exists(store.connection);
    should(store.connection).be.an.instanceOf(store.Connection);
  });

  it('mirror del metodo `touch`', function (done) {
    should.exists(store.touch);
    should(store.touch).be.a.Function();

    store.touch('type', 'value', function (err, result) {
      should.exists(result);
      should(result).be.a.String();
      token = result;
      done();
    });
  });

  it('mirror del metodo `check`', function (done) {
    should.exists(store.check);
    should(store.check).be.a.Function();

    store.check('type', token, function (err, result) {
      should.exists(result);
      should(result).be.a.String();
      should(result).be.equal('value');
      done();
    });
  });
});