/**
 * Created by dzambuto on 15/09/15.
 */
var Connection = require('./connection');


/**
 * Temporary constructor.
 * @param options
 * @constructor
 */
function Temporary (options) {
  this.connectiones = [];
  this.createConnection(options);
}

/**
 * Creates a Connection instance.
 * @param options
 */
Temporary.prototype.createConnection = function (options) {
  var connection = new Connection(options);
  this.connectiones.push(connection);
  return connection;
};

/**
 * The default connection of the temporary module.
 * @property connection
 * @return {Connection}
 * @api public
 */
Temporary.prototype.__defineGetter__('connection', function() {
  return this.connectiones[0];
});

/**
 * Connection methods.
 * @api public
 */
Temporary.prototype.touch = function () {
  var connection = this.connection;
  connection.touch.apply(connection, arguments);
};

Temporary.prototype.check = function () {
  var connection = this.connection;
  connection.check.apply(connection, arguments);
};

/**
 * Temporary constructor.
 * @type {Temporary}
 * @api public
 */
Temporary.prototype.Temporary = Temporary;

/**
 * Connection constuctor.
 * @type {exports|module.exports}
 * @api public
 */
Temporary.prototype.Connection = Connection;

/**
 * The exports object is an instance of Temporary.
 * @type {Temporary}
 * @api public
 */
var temporary = module.exports = exports = new Temporary;
