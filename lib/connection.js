/**
 * Created by dzambuto on 02/10/15.
 */
var debug = require('debug')('micro:connection');
var Redis = require('ioredis');
var uuid = require('node-uuid');
var util = require('util');
var EventEmitter = require('events');
var config = require('config');

var default_port = 6379;
var default_host = '127.0.0.1';
var noop = function(){};

var oneDay = 86400;

if (config.has('main.redis')) {
  var redis = config.get('main.redis');
  default_host = redis.host;
  default_port = redis.port;
}

function Connection (options) {
  options = options || {};

  EventEmitter.call(this);

  this.prefix = options.prefix == null
    ? 'token:'
    : options.prefix;


  this.client = new Redis(
    options.port || default_port,
    options.host || default_host
  );

  this.ttl = options.ttl;
}

util.inherits(Connection, EventEmitter)

Connection.prototype.touch = function (type, uid, fn) {
  var connection = this;
  var token = uuid.v4();
  var key = connection.prefix + type + ':' + token;

  if (!fn) fn = noop;

  var ttl = connection.ttl || oneDay;

  debug('TOUCH "%s"', key);

  connection.client.setex(key, ttl, uid, function (err) {
    if (err) return fn(err);
    debug('TOUCH completed');
    return fn(null, token);
  });
};

Connection.prototype.check = function (type, token, fn) {
  var connection = this;
  var key = connection.prefix + type + ':' + token;

  if (!fn) fn = noop;

  debug('CHECK "%s"', key);

  connection.client.get(key, function (err, data) {
    if (err) return fn(err);
    if (!data) return fn(null, false);
    debug('CHECK completed');
    return fn(null, data);
  });
};

module.exports = Connection;